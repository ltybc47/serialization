﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Serialization.Json;
using System.Xml.Serialization;

namespace SerializationS
{
    class Program
    {
        static void Main(string[] args)
        {
            var characters = new List<Character>();

            var people = new List<Person>();

            
            for (int i = 0; i < 5; i++)
            {
                characters.Add(new Character());

                var person = new Person()
                {
                    Character = characters[i]
                };
                people.Add(person);
            }

            foreach (var item in people)
            {
                Console.WriteLine($"{item}{item.Character}");
            }

            SaveSerialization save = new SaveSerialization();

            //Одновременно все методы сериализации запускать нельзя

            save.SaveBinaryFormatter(people);

            //save.SaveXmlSerializer(people);

            //save.SaveJsonSerializer(characters);

            //save.SaveSoapSerializer(characters);

            Console.ReadLine();
        }
    }
}
