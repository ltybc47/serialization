﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerializationS
{
    class RandomPeople
    {
        private static readonly Random Random = new Random();

        private static readonly Dictionary<int, string> LastName = new Dictionary<int, string>()
        {
            { 1, "Иванов" }, {2, "Смирнов" }, {3, "Кузнецов" }, {4, "Попов" }, {5, "Васильев" }, {6, "Петров" }, {7, "Соколов" }, {8, "Михайлов" }, {9, "Новиков" }, {10, "Федоров" }
        };

        private static readonly Dictionary<int, string> FirstName = new Dictionary<int, string>()
        {
            { 1, "Александр" }, {2, "Иван" }, {3, "Пётр" }, {4, "Руслан" }, {5, "Сергей" }, {6, "Тимофей" }, {7, "Михаил" }, {8, "Николай" },  {9, "Евгений" }, {10, "Виталий" }
        };

        private static readonly Dictionary<int, string> Speciality = new Dictionary<int, string>()
        {
            { 1, "Артист" }, {2, "Ветеринар" }, {3, "Врач" }, {4, "Геолог" }, {5, "Кинолог" }, {6, "Переводчик" }, {7, "Репортер" }, {8, "Учитель" },  {9, "Экономист" }, {10, "Юрист" }
        };

        public static Guid GetGuid() => Guid.NewGuid();

        public static string GetRandomLastName() => LastName[Random.Next(1, LastName.Count + 1)];
        public static string GetRandomFirstName() => FirstName[Random.Next(1, FirstName.Count + 1)];
        public static string GetRandomSpeciality() => Speciality[Random.Next(1, Speciality.Count + 1)];
        public static int GetRandomAge() => Random.Next(25, 50);

        public static string GetLastName() => RandomPeople.GetRandomLastName();
        public static string GetFirstName() => RandomPeople.GetRandomFirstName();
        public static string GetSpeciality() => RandomPeople.GetRandomSpeciality();
        public static int GetAge() => RandomPeople.GetRandomAge();
    }
}
