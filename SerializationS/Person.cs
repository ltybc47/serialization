﻿using System;
using System.Runtime.Serialization;

namespace SerializationS
{
    [Serializable]   
    public class Person
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Speciality { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age { get; set; }
        public Character Character { get; set; }

        public Person()
        {
            LastName = RandomPeople.GetLastName();
            FirstName = RandomPeople.GetFirstName();
            Speciality = RandomPeople.GetRandomSpeciality();
            Age = RandomPeople.GetAge();
            BirthDate = DateTime.Today.AddYears(-Age);
        }

        public Person(string lastName, string firstName, int age)
        {
            LastName = lastName;
            FirstName = firstName;
            Speciality = RandomPeople.GetRandomSpeciality();
            BirthDate = DateTime.Today.AddYears(-age);
            Age = age;
        }

        [NonSerialized]
        public Guid Id = RandomPeople.GetGuid();

        public override string ToString()
        {
            return $"{LastName} {FirstName}\nПрофессия: {Speciality}\nГод рождения: {BirthDate.Year}\nВозраст: {Age}\n";
        }
    }
}
